//
//  SignupEmailViewController.m
//  LIVEASE
//
//  Created by AOC on 23/08/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "SignupEmailViewController.h"

@interface SignupEmailViewController ()
@property (weak, nonatomic) IBOutlet UIView *vEmail;
@property (weak, nonatomic) IBOutlet UIView *vPhone;
@property (weak, nonatomic) IBOutlet UIView *vName;
@property (weak, nonatomic) IBOutlet UIView *vPassword;
@property (weak, nonatomic) IBOutlet UIView *vRePassword;

@end

@implementation SignupEmailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    
    self.vEmail.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.vPhone.layer.borderColor
    = [[UIColor lightGrayColor] CGColor];
    
    self.vName.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.vPassword.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.vRePassword.layer.borderColor = [[UIColor lightGrayColor] CGColor];
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
