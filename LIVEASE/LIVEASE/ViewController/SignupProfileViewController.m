//
//  SignupProfileViewController.m
//  LIVEASE
//
//  Created by AOC on 23/08/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "SignupProfileViewController.h"

@interface SignupProfileViewController ()
@property (weak, nonatomic) IBOutlet UIView *vBirthday;
@property (weak, nonatomic) IBOutlet UIView *vGender;
@property (weak, nonatomic) IBOutlet UIImageView *imvPhoto;

@end

@implementation SignupProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.imvPhoto.layer.cornerRadius = self.imvPhoto.frame.size.height / 2;
    
    self.vBirthday.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.vGender.layer.borderColor = [[UIColor lightGrayColor] CGColor];
}
- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
