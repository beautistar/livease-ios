//
//  LoginViewController.m
//  LIVEASE
//
//  Created by AOC on 22/08/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "LoginViewController.h"
#import "SignupEmailViewController.h"
#import "SelServiceViewController.h"
#import "Const.h"

@interface LoginViewController () {
    
    int _status;
    
    
}

@property (weak, nonatomic) IBOutlet UITextField *txfEmail;
@property (weak, nonatomic) IBOutlet UITextField *txfPassword;
@property (weak, nonatomic) IBOutlet UIView *vDiv;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *posDivView;
@property (weak, nonatomic) IBOutlet UIImageView *imvSigninTragle;
@property (weak, nonatomic) IBOutlet UIImageView *imvSignupTragle;

@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;


@end

@implementation LoginViewController

@synthesize txfEmail, txfPassword;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    
    [self initView];
    
}

- (void) initView {
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    txfEmail.leftView = paddingView1;
    txfEmail.leftViewMode = UITextFieldViewModeAlways;
    txfEmail.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    
    txfPassword.leftView = paddingView2;
    txfPassword.leftViewMode = UITextFieldViewModeAlways;
    txfPassword.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _status = SIGNIN_STATUS;// signin status
    self.imvSignupTragle.hidden = YES;
    
    
}
- (IBAction)signinAction:(id)sender {
    
    if (_status != SIGNIN_STATUS) {
    
        [txfPassword setHidden:NO];
        
        self.posDivView.constant = self.posDivView.constant + 50;
        
        _status = SIGNIN_STATUS;
        
        self.imvSigninTragle.hidden = NO;
        self.imvSignupTragle.hidden = YES;
        [self.btnSignIn setTitleColor:[UIColor colorWithRed:159/255.0 green:114/255.0 blue:229/255.0 alpha:1.0] forState:UIControlStateNormal];
        [self.btnSignUp setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
    }
}


- (IBAction)signupAction:(id)sender {
    
    if (_status != SIGNUP_STATUS) {
    
        [txfPassword setHidden:YES];
        
        self.posDivView.constant = self.posDivView.constant - 50;
        
        _status = SIGNUP_STATUS;
        
        self.imvSigninTragle.hidden = YES;
        self.imvSignupTragle.hidden = NO;
        self.btnSignUp.tintColor = [UIColor colorWithRed:159/255.0 green:114/255.0 blue:229/255.0 alpha:1.0];
        self.btnSignIn.tintColor = [UIColor grayColor];
        
        [self.btnSignUp setTitleColor:[UIColor colorWithRed:159/255.0 green:114/255.0 blue:229/255.0 alpha:1.0] forState:UIControlStateNormal];
        [self.btnSignIn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
    }
    
}

- (IBAction)nextAction:(id)sender {
    
    if (_status == SIGNIN_STATUS) {
        
        [self gotoService];
    } else {
        [self gotoSignupForEmail];    }
}

- (void) gotoSignupForEmail {
    
    SignupEmailViewController *seVC = (SignupEmailViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SignupEmailViewController"];
    [self.navigationController pushViewController:seVC animated:YES];
    
}

- (void) gotoService {
    
    UINavigationController *sevNavVC = (UINavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"SeviceNavViewController"];
    //[self.navigationController pushViewController:sevNavVC animated:YES];
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:sevNavVC];
    
    
}


@end
