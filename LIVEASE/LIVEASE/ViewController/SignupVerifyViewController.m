//
//  SignupVerifyViewController.m
//  LIVEASE
//
//  Created by AOC on 23/08/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "SignupVerifyViewController.h"

@interface SignupVerifyViewController ()
@property (weak, nonatomic) IBOutlet UIView *vAuthCode;

@end

@implementation SignupVerifyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    
    self.vAuthCode.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
