//
//  JobDesViewController.m
//  LIVEASE
//
//  Created by AOC on 24/08/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "JobDesViewController.h"

@interface JobDesViewController ()
@property (weak, nonatomic) IBOutlet UITextView *txvHire;
@property (weak, nonatomic) IBOutlet UITextView *txvWork;

@end

@implementation JobDesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.txvHire.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    self.txvWork.layer.borderColor = [[UIColor lightGrayColor] CGColor];
}



@end
